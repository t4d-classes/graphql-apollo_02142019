# Welcome to the GraphQL and Apollo Class

## Instructor

Eric Greene

## Schedule

Class:

- Thursday - Friday, 9am to 3pm PST

Breaks:

- Morning: 10:20am to 10:30am
- Lunch: 11:45am to 12:15pm
- Afternoon: 1:35pm to 1:45pm

## Course Outline

- Day 1 - Overview of GraphQL, Queries, Query Component, Mutations
- Day 2 - Mutation Component, Apollo Link, Client Side State

### Requirements

- Node.js (version 10 or later)
- Web Browser
- Text Editor

### Instructor's Resources

- [DevelopIntelligence](http://www.developintelligence.com/)
