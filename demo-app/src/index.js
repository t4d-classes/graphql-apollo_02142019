import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';

import { ApolloProvider } from 'react-apollo';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { ApolloClient } from 'apollo-client';
import { withClientState } from 'apollo-link-state';
import { ApolloLink } from 'apollo-link';
import gql from 'graphql-tag';


const GRAPHQL_PORT = process.env.REACT_APP_GRAPHQL_PORT || 3010;

const cache = new InMemoryCache();

const clientStateLink = withClientState({
  cache,
  defaults: {
    widgetToolName: 'Widget Tool!!',
    editWidgetId: -1,
  },
  resolvers: {
    Mutation: {
      setEditWidgetId: (_, { widgetId }, { cache }) => {

        const editWidgetIdQuery = gql`
          query EditWidgetIdQuery {
            editWidgetId
          }
        `;

        cache.writeQuery({ query: editWidgetIdQuery, data: { editWidgetId: widgetId }});
      },
    },
  },
});

const httpLink = new HttpLink({
  uri: `http://localhost:${GRAPHQL_PORT}/graphql`,
});

const client = new ApolloClient({
  link: ApolloLink.from([ clientStateLink, httpLink ]),
  cache, connectToDevTools: true,
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root'),
);
