import React from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

import { EditWidgetMutation } from './EditWidgetMutation';

const cancelWidgetMutation = gql`
  mutation CancelWidget($widgetId: Int!) {
    setEditWidgetId(widgetId: $widgetId) @client
  }
`;

export const CancelWidgetMutation = (props) => {

  return <Mutation mutation={cancelWidgetMutation}>
    {mutate => {

      const cancelWidget = () => {

        mutate({
          variables: { widgetId: -1 },
        }).then(results => {
          console.log('success: ', results);
        }).catch(results => {
          console.log('error',results);
        });

      };

      return <EditWidgetMutation onCancelWidget={cancelWidget} {...props} />;

    }}
  </Mutation>;

};