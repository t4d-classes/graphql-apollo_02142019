import React from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

import { WidgetTableQuery } from '../../queries/widgets/WidgetTableQuery';

const widgetsQuery = gql`
  query Widgets {
    widgets {
      id
      name
      description
      quantity
      color
      price
    }
  }
`;

const deleteWidgetMutation = gql`
  mutation DeleteWidget($widgetId: Int!) {
    deleteWidget(widgetId: $widgetId) {
      id
      name
      description
      quantity
      color
      price
    }
  }
`;

export const DeleteWidgetMutation = (props) => {

  return <Mutation mutation={deleteWidgetMutation}>
    {mutate => {

      const deleteWidget = widgetId => {

        mutate({
          variables: { widgetId },
          optimisticResponse: {
            deleteWidget: {
              id: widgetId,
              __typename: 'Widget'
            },
          },
          update: (store, { data: { deleteWidget: widget }}) => {
            // local cache update
            // will run twice: once for optimistic and one for real save
            let data = store.readQuery({ query: widgetsQuery });
            // data.widgets.push(widget);
            let newWidgets = data.widgets.filter(w => w.id !== widget.id);
            store.writeQuery({ query: widgetsQuery, data: { widgets: newWidgets } });
          },
        });

      };

      return <WidgetTableQuery onDeleteWidget={deleteWidget} {...props} />;

    }}
  </Mutation>;

};