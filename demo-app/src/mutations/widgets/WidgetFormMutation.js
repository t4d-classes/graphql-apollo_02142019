import React from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

import { WidgetForm } from '../../components/widgets/WidgetForm';

const widgetsQuery = gql`
  query Widgets {
    widgets {
      id
      name
      description
      quantity
      color
      price
    }
  }
`;

const appendWidgetMutation = gql`
  mutation AppendWidget($widget: AppendWidget!) {
    appendWidget(widget: $widget) {
      id
      name
      description
      quantity
      color
      price
    }
  }
`;

export const WidgetFormMutation = () => {

  return <Mutation mutation={appendWidgetMutation}>
    {mutate => {

      const appendWidget = widget => {

        mutate({
          // mutation: appendWidgetMutation,
          variables: { widget },
          optimisticResponse: {
            appendWidget: {
              id: -1,
              ...widget,
              __typename: 'Widget'
            },
          },
          update: (store, { data: { appendWidget: widget }}) => {
            // local cache update
            // will run twice: once for optimistic and one for real save
            let data = store.readQuery({ query: widgetsQuery });
            // data.widgets.push(widget);
            let newWidgets = data.widgets.concat(widget);
            store.writeQuery({ query: widgetsQuery, data: { widgets: newWidgets } });
          },
        });

      };

      return <WidgetForm buttonText="Add Widget" onSubmitWidget={appendWidget} />;

    }}
  </Mutation>;

};