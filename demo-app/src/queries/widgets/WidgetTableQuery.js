import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import { WidgetTable } from '../../components/widgets/WidgetTable';

const widgetsQuery = gql`
  query Widgets {
    editWidgetId @client
    widgets {
      id
      name
      description
      quantity
      color
      price
    }
  }
`;

export const WidgetTableQuery = (props) => {

  return <Query query={widgetsQuery}>
    {( { loading, error, data } ) => {

      if (loading) return 'Loading';
      if (error) return 'Error';

      return <WidgetTable widgets={data.widgets} editWidgetId={data.editWidgetId} {...props} />;
    }}
  </Query>;


};