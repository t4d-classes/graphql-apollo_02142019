import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import { ToolHeader } from '../../components/shared/ToolHeader';

const toolHeaderQuery = gql`
  query ToolHeader {
    widgetToolName @client
  }
`;

export const WidgetToolHeaderQuery = (props) => {

  return <Query query={toolHeaderQuery}>
    {( { loading, error, data } ) => {

      if (loading) return 'Loading';
      if (error) return 'Error';

      console.log(data);

      return <ToolHeader headerText={data.widgetToolName} {...props} />;
    }}
  </Query>;

};